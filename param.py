# Positional Arguments
# def sum(n1,n2):
#     return(n1+n2)

# a=sum(10,20)

# keyword Arguments
def sub(n1,n2):
    return(n1-n2)

# a=sub(n2=10,n1=20)
a=sub(10,n2=20)          
print(a)