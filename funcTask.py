a='''---------------------
Press 1. To Create Account
Press 2. To Check Your balance
Press 3. To Deposit
Press 4. To Withdraw
Press 0. To Exit'''

all=[]
import random
def create_account():
    nm=input('Please Enter Your Name: ')
    email=input('Enter Your Email Address: ')
    amt=int(input('Enter Initial Amount: '))
    acc_no=random.randint(100000,999999)
    user={
        'name':nm,
        'email':email,
        'amount':amt,
        'acc':acc_no
    }
    print('Account Created Successfully!!! Your Account No. is:',user['acc'])
    all.append(user)

def check_balance():
    bal=int(input('Enter Your Account Number: '))
    for i in all:
        if i['acc']==bal:
            print('Welcome',i['name'],'!!!')
            print('Your Current Balance is:',i['amount'])
        
def deposit():
    bal=int(input('Enter Your Account Number: '))
    dep=0   
    for i in all:
        if i['acc']==bal:
            print('Welcome Back',i['name'],'!!!')   
            dep=int(input('Enter Amount to Deposit: '))
            i['amount']+=dep      
            print('Your Account is Credited with Rs: {}/- and Your New Balance is: {}'.format(dep,i['amount']))

def withdraw():
    bal=int(input('Enter Your Account Number: '))
    dep=0   
    for i in all:
        if i['acc']==bal:
            print('Welcome Back',i['name'],'!!!')   
            dep=int(input('Enter Amount to Withdraw: '))
            i['amount']-=dep      
            print('Your New Balance is:',i['amount'])    

while True:
    print(a)
    choice=int(input('Choose an Action: '))
    if choice==1:
        create_account()
    elif choice==2:
        check_balance()
    elif choice==3:
        deposit()
    elif choice==4:
        withdraw()
    elif choice==0:
        break     
    else:
        print('Wrong Choice!!! Try Again')